import { Module } from '@nestjs/common';
import { PersonsModule } from './api/domain/persons/persons.module';
import { AddressModule } from './api/domain/address/address.module';
import { ConfigModule } from '@nestjs/config';
import { DatabaseModule } from './db/database.module';

@Module({
  controllers: [],
  providers: [],
  imports: [PersonsModule, 
            AddressModule, 
            ConfigModule.forRoot({ isGlobal: true }),
            DatabaseModule
           ],
})
export class AppModule {}
