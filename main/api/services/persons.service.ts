import { Injectable, Inject } from '@nestjs/common';
import { CreatePersonDto } from '../domain/persons/entities/dto/create-person.dto';
import { UpdatePersonDto } from '../domain/persons/entities/dto/update-person.dto';
import { Person } from '../domain/persons/entities/person.entity';

@Injectable()
export class PersonsService {

  constructor(
    @Inject('PERSON_REPOSITORY')
    private personRepository: typeof Person
  ) {}

  create(createPersonDto: CreatePersonDto) {
    return this.personRepository.create<Person>(createPersonDto);
  }

  findAll() {
    return this.personRepository.findAll<Person>();
  }

  findOne(id: number) {
    return this.personRepository.findOne({where : {id: id} })
  }

  update(id: number, updatePersonDto: UpdatePersonDto) {
    return this.personRepository.update(updatePersonDto, {where: {id: id}})
  }

  remove(id: number) {
    return this.personRepository.destroy({where: {id:id}})
  }
}
