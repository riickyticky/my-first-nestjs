export class CreatePersonDto {
     name: string;
     lastName: string;
     age: number;
}
