import { Module } from '@nestjs/common';
import { PersonsService } from '../../services/persons.service';
import { PersonsController } from '../../controllers/persons.controller';
import { personRepository } from '../../repositories/personRepository';
import { DatabaseModule } from '../../../db/database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [PersonsController],
  providers: [PersonsService, ...personRepository]
})
export class PersonsModule {}
