import { Table, Column, Model } from 'sequelize-typescript';

@Table
export class Address extends Model {
  @Column
  nameAddress: string;
}