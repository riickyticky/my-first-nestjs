import { Person } from '../domain/persons/entities/person.entity';

export const personRepository = [
  {
    provide: 'PERSON_REPOSITORY',
    useValue: Person,
  },
];