import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { PersonsService } from '../services/persons.service';
import { CreatePersonDto } from '../domain/persons/entities/dto/create-person.dto';
import { UpdatePersonDto } from '../domain/persons/entities/dto/update-person.dto';

@Controller('persons')
export class PersonsController {
  constructor(private readonly personsService: PersonsService) {}

  @Post("/create")
  create(@Body() createPersonDto: CreatePersonDto) {
    return this.personsService.create(createPersonDto);
  }

  @Get("/getAll")
  findAll() {
    return this.personsService.findAll();
  }

  @Get('/getById/:id')
  findOne(@Param('id') id: string) {
    return this.personsService.findOne(+id);
  }

  @Patch('/update/:id')
  update(@Param('id') id: string, @Body() updatePersonDto: UpdatePersonDto) {
    return this.personsService.update(+id, updatePersonDto);
  }

  @Delete('/delete/:id')
  remove(@Param('id') id: string) {
    return this.personsService.remove(+id);
  }
}
