import { Module } from '@nestjs/common';
import { databaseInit } from './database.init';

@Module({
    providers: [...databaseInit],
    exports: [...databaseInit],
})
export class DatabaseModule {}