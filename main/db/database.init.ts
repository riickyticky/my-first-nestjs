import { Sequelize } from 'sequelize-typescript';
import { SEQUELIZE, DEV, TEST, PRODUCTION } from './config/constants';
import { databaseConfig } from './config/config';
import { Person } from '../api/domain/persons/entities/person.entity';
import { Address } from '../api/domain/address/entities/address.entity';

export const databaseInit = [{
    provide: SEQUELIZE,
    useFactory: async () => {
        let config;
        switch (process.env.NODE_ENV) {
        case DEV:
           config = databaseConfig.dev;
           break;
        case TEST:
           config = databaseConfig.test;
           break;
        case PRODUCTION:
           config = databaseConfig.production;
           break;
        default:
           config = databaseConfig.dev;
        }
        const sequelize = new Sequelize(config);
        sequelize.addModels([Person, Address]);
        await sequelize.sync();
        return sequelize;
    },
}];