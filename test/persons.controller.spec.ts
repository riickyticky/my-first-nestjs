import { Test, TestingModule } from '@nestjs/testing';
import { PersonsController } from '../main/api/controllers/persons.controller';
import { PersonsService } from '../main/api/services/persons.service';

describe('PersonsController', () => {
  let controller: PersonsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PersonsController],
      providers: [PersonsService],
    }).compile();

    controller = module.get<PersonsController>(PersonsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
